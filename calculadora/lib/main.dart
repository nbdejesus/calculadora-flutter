import 'package:calculadora/button.dart';
import 'package:flutter/material.dart';
import 'package:math_expressions/math_expressions.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  var userQuestion = '';
  var userAnswer = '';
  final List<String> buttons = [
    'C',
    '+/-',
    '%',
    '/',
    '7',
    '8',
    '9',
    'x',
    '4',
    '5',
    '6',
    '-',
    '1',
    '2',
    '3',
    '+',
    '0',
    ' ',
    '.',
    '=',
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[400],
      body: Column(
        children: <Widget>[
          Expanded(
            child: Container(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  SizedBox(height: 50,),
                  Container(
                    padding: EdgeInsets.all(20),
                    alignment: Alignment.centerRight,
                    child: Text(userQuestion,style: TextStyle(fontSize: 30),),
                    ),
                    Container(
                    padding: EdgeInsets.all(20),
                    alignment: Alignment.centerRight,
                    child: Text(userAnswer,style: TextStyle(fontSize: 30),),
                    ),
              ],),
            ),
          ),
          Expanded(           
            flex: 2,
            child: Container(
            color: Colors.grey[100],
            padding: EdgeInsets.all(30),
                child: GridView.builder(
                    itemCount: buttons.length,
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 4),
                    itemBuilder: (BuildContext context, int index) {
                      //Clear Button
                      if(index == 0){
                        return MyButton( 
                           buttonTapped:(){
                          setState(() {
                            userQuestion = '';
                          }); 
                        },
                        buttonText: buttons[index],
                        color: Colors.grey[400],
                        textColor: Colors.white,
                        );                      
                      }

                      //Equal button
                     else if(index == buttons.length - 1){
                       return MyButton(
                         buttonTapped:(){
                          setState((){
                            equalPressed();
                        });
                         },
                       buttonText: buttons[index],
                        color: Colors.grey[700],
                        textColor: Colors.white,
                       );
                     }

                      return MyButton(
                        buttonTapped: (){
                          setState(() {
                            userQuestion += buttons[index];
                          });
                        },
                        buttonText: buttons[index],
                        color: isOperator(buttons[index])
                            ? Colors.grey[400]
                            : Colors.grey[100],
                        textColor: isOperator(buttons[index])
                            ? Colors.white
                            : Colors.black,
                      );
                    })),
          )
        ],
      ),
    );
  }

  bool isOperator(String x) {
    if (x == 'C' ||
        x == '+/-' ||
        x == '%' ||
        x == '/' ||
        x == 'x' ||
        x == '-' ||
        x == '+' ||
        x == '=') {
      return true;
    }
    return false;
  }
  void equalPressed(){
    String finalQuestion = userQuestion;
finalQuestion = finalQuestion.replaceAll('x','*');
    Parser p = Parser();
    Expression exp = p.parse(finalQuestion);
    ContextModel cm = ContextModel();
    double eval = exp.evaluate(EvaluationType.REAL,cm);
    userAnswer =eval.toString();
  }
}
